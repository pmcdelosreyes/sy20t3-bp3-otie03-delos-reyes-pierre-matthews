#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"

class Player :
	public GameObject
{
public:
	~Player();
	void start();
	void update();
	void draw();

	void setIsColliding(bool colliding);
	void setPositionY(int yPos);

	int getPositionX();
	int getPositionY();
	int getWidth();
	int getHeight();
	int getIsAlive();

	void doDeath();

private:
	SDL_Texture* texture;
	Mix_Chunk* sound; 
	int x, y;
	float speed;
	int width, height;

	// determine if player is alive
	bool isAlive;
	bool isColliding;

};

