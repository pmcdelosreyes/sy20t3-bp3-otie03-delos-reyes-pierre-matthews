#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player();
	this->addGameObject(player);

	points = 0;
}

GameScene::~GameScene()
{
	delete player;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	texture = loadTexture("gfx/Final Cave Background.PNG");
	initFonts();
	SDL_QueryTexture(texture, NULL, NULL, 0, 0);

	currentSpawnTimer = 120;
	spawnTime = 60; // Spawn time of 1 second (60 frames per second)

	for (int i = 0; i < 1; i++)
	{
		spawn();
	}
}

void GameScene::draw()
{
	Scene::draw();

	drawText(150, 20, 255, 255, 255, TEXT_CENTER, "FEET FELL: %03d", points);

	// Death scene
	if (player->getIsAlive() == false)
	{
		drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 255, 255, 255, TEXT_CENTER, "GAME OVER!");
	}
}

void GameScene::update()
{
	Scene::update();
	blit(texture, 0, 0);
	doSpawnLogic();
	doCollisionLogic();
}

void GameScene::doSpawnLogic()
{
	if (currentSpawnTimer > 0)
		currentSpawnTimer--;

	if (player->getIsAlive() && currentSpawnTimer <= 0)
	{
		spawn();
	}
}

void GameScene::doCollisionLogic()
{
	bool hasCollided = false;
	bool addedPoint = false;

	for (int i = 0; i < objects.size(); i++)
	{
		Platform* platform = dynamic_cast<Platform*>(objects[i]);

		if (platform != NULL)
		{
			int collision = checkCollision(
			player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
			platform->getPositionX(), platform->getPositionY(), platform->getWidth(), platform->getHeight()
			);
			if (collision == 0 && player->getIsAlive()) // While not getting hit
			{
				points++;
				addedPoint = true;
			}
			else // If hit by platform = lose
			{
				hasCollided = true;
				player->setPositionY(platform->getPositionY() - platform->getHeight() / 2);
				player->doDeath();
			}
		}
	}
	player->setIsColliding(hasCollided);
	despawnPlatform();

	// Also lose if player goes out of range map
	if (player->getPositionY() < 0 || player->getPositionY() > 640 || player->getPositionX() < 0 || player->getPositionX() > 480)
	{
		player->doDeath();
	}
}

void GameScene::spawn()
{
	Platform* platform = new Platform();
	this->addGameObject(platform);

	platform->setPosition(0 + (rand() % 300), 600);
	spawnedPlatforms.push_back(platform); 
	currentSpawnTimer = spawnTime;
}

void GameScene::despawnPlatform()
{
	Platform* toDeletePlatform = NULL;
	int index = -1;
	for (int i = 0; i < spawnedPlatforms.size(); i++)
	{
		if (spawnedPlatforms[i]->getPositionY() < 0)
		{
			index = i;
			toDeletePlatform = spawnedPlatforms[i];
			break;
		}
	}
	
	if (index != -1)
	{
		spawnedPlatforms.erase(spawnedPlatforms.begin() + index);
		delete toDeletePlatform;
	}
}


