#include "Player.h"

Player::~Player()
{
}

void Player::start()
{
	texture = loadTexture("gfx/Umbrella down.png");

	// Initialize variables
	x = 225;
	y = 100;
	width = 0;
	height = 0;
	speed = 1;
	isAlive = true;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void Player::update()
{
	if (isAlive)
	{
		if (app.keyboard[SDL_SCANCODE_A])
		{
			texture = loadTexture("gfx/Umbrella Left.png");
			x -= speed * 3;
		}

		if (app.keyboard[SDL_SCANCODE_D])
		{
			texture = loadTexture("gfx/Umbrella Right.png");
			x += speed * 3;
		}
	}
}

void Player::draw()
{
	if (isAlive)
	{
		blit(texture, x, y);
	}
}

void Player::setIsColliding(bool colliding)
{
	isColliding = colliding;
}

void Player::setPositionY(int yPos)
{
	y = yPos;
}

int Player::getPositionX()
{
	return x;
}

int Player::getPositionY()
{
	return y;
}

int Player::getWidth()
{

	return width;
}

int Player::getHeight()
{

	return height;
}

int Player::getIsAlive()
{
	return isAlive;
}

void Player::doDeath()
{
	isAlive = false;
}
