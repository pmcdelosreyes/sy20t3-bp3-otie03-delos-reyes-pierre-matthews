#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "util.h"

class Platform :
    public GameObject
{
public:
	Platform();
	~Platform();
	void start();
	void update();
	void draw();

	void setPosition(int xPos, int yPos);

	int getPositionX();
	int getPositionY();
	int getWidth();
	int getHeight();
	int getSpeed();

private:
	int x;
	int y;
	int speed;
	int width;
	int height;
	int directionX;
	int directionY;
	int directionChangeTime;
	int currentDirectionChangeTime;
	SDL_Texture* texture;
};

