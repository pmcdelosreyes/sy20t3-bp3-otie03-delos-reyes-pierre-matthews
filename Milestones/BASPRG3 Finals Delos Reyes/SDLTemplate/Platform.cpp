#include "Platform.h"

Platform::Platform()
{
}
Platform::~Platform()
{
}

void Platform::start()
{
	texture = loadTexture("gfx/Death Platform.png");

	// Initialize variables
	directionX = -1; 
	directionY = 1;
	width = 0;
	height = 0;
	speed = 3;
	directionChangeTime = (rand() % 100) + 400;
	currentDirectionChangeTime = 0;

	// Query texture to set width and height
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void Platform::update()
{
	y -= directionY * speed; 
}

void Platform::draw()
{
	blit(texture, x, y);
}

void Platform::setPosition(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

int Platform::getPositionX()
{
	return x;
}

int Platform::getPositionY()
{
	return y;
}

int Platform::getWidth()
{
	return width;
}

int Platform::getHeight()
{
	return height;
}

int Platform::getSpeed()
{
	return speed;
}
