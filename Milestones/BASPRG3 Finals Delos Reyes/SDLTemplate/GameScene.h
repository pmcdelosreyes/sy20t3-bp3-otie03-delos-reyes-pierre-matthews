#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Player.h"
#include "Platform.h"
#include <vector>
#include "text.h"
#include "SoundManager.h"

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();

private:
	Player* player;

	// Platform spawn logic
	float spawnTime;
	float currentSpawnTimer;
	std::vector<Platform*> spawnedPlatforms;
	SDL_Texture* texture; // pointer for texture to be loaded
	Mix_Chunk* sound; // mixer library pointer for a sound file 

	void doSpawnLogic();
	void doCollisionLogic();
	void spawn();
	void despawnPlatform();

	int points;
};