#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player;
	this->addGameObject(player);

	points = 0;
}

GameScene::~GameScene()
{
	delete player;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	initFonts();
	currentSpawnTimer = 300;
	spawnTime = 300; // Spawn time of 5 seconds

	for (int i = 0; i < 3; i++)
	{
		spawn();
	}
}

void GameScene::draw()
{
	Scene::draw();

	drawText(110, 20, 255, 255, 255, TEXT_CENTER, "POINTS: %03d", points);

	if (player->getIsAlive() == false)
	{
		drawText(SCREEN_WIDTH / 2, 600, 255, 255, 255, TEXT_CENTER, "GAME OVER!");
	}
}

void GameScene::update()
{
	Scene::update();
	doSpawnLogic();
	doCollisionLogic();
	doPowerupCollision();
	
}

void GameScene::doSpawnLogic()
{
	if (currentSpawnTimer > 0)
		currentSpawnTimer--;

	if (currentSpawnTimer <= 0)
	{
		for (int i = 0; i < 3; i++)
		{
			spawn();
		}
		spawnPowerup();
		currentSpawnTimer = spawnTime;
	}
}

void GameScene::doCollisionLogic()
{
	// Check for collision
	for (int i = 0; i < objects.size(); i++)
	{
		// Cast to bullet
		Bullet* bullet = dynamic_cast<Bullet*>(objects[i]);

		// Check if the cast was successful (i.e objects[i] is a bullet)
		if (bullet != NULL)
		{
			// If the bullet is from the enemy side, check against the player
			if (bullet->getSide() == Side::ENEMY_SIDE)
			{
				int collision = checkCollision(
					player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
					bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
				);

				if (collision == 1)
				{
					player->doDeath();
					break;
				}
			}
			// If the bullet is from the player side, check against ALL enemies
			else if (bullet->getSide() == Side::PLAYER_SIDE)
			{
				for (int i = 0; i < spawnedEnemies.size(); i++)
				{
					Enemy* currentEnemy = spawnedEnemies[i];

					int collision = checkCollision(
						currentEnemy->getPositionX(), currentEnemy->getPositionY(), currentEnemy->getWidth(), currentEnemy->getHeight(),
						bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
					);
					
					if (collision == 1)
					{
						despawnEnemy(currentEnemy);
						// Increment points
						points++;
						// IMPORTANT: only despawn one at at time, otherwise we might crash
						break;
					}
				}
			}
		}
	}
	for (int i = 0; i < spawnedEnemies.size(); i++)
	{
		Enemy* currentEnemy = spawnedEnemies[i];
		if (currentEnemy->getPositionY() > SCREEN_HEIGHT - 20)
		{
			despawnEnemy(currentEnemy);
			break;
		}
	}
}

void GameScene::doPowerupCollision()
{
	for (int i = 0; i < objects.size(); i++)
	{
		Powerup* powerup = dynamic_cast<Powerup*>(objects[i]);
		if (powerup != NULL)
		{
			int collision = checkCollision(
				player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
				powerup->getPositionX(), powerup->getPositionY(), powerup->getWidth(), powerup->getHeight()
			);
			for (int i = 0; i < spawnedPowerups.size(); i++)
			{
				Powerup* currentPowerup = spawnedPowerups[i];
				if (collision == 1)
				{
					player->getAddBullet(1);
					despawnPowerup(currentPowerup);
					break;
				}
			}
		}
	}
}

void GameScene::spawn()
{
	Enemy* enemy = new Enemy();
	this->addGameObject(enemy);
	enemy->setPlayerTarget(player);

	enemy->setPosition(300 + rand() % 700, -400 + rand() % 400);
	spawnedEnemies.push_back(enemy);
}

void GameScene::despawnEnemy(Enemy* enemy)
{
	int index = 0;
	for (int i = 0; i < spawnedEnemies.size(); i++)
	{
		// If the pointer matches
		if (enemy == spawnedEnemies[i])
		{
			index = i;
			break;
		}
	}

	// If any match is found
	if (index != -1)
	{
		spawnedEnemies.erase(spawnedEnemies.begin() + index);
		delete enemy;
	}
}

void GameScene::spawnPowerup()
{
	Powerup* powerup = new Powerup();
	this->addGameObject(powerup);

	powerup->setPosition(300 + rand() % 700, -400 + rand() % 400);
	spawnedPowerups.push_back(powerup);
}

void GameScene::despawnPowerup(Powerup* powerup)
{
	int index = 0;
	for (int i = 0; i < spawnedPowerups.size(); i++)
	{
		if (powerup == spawnedPowerups[i])
		{
			index = i;
			break;
		}
	}
	if (index != -1)
	{
		spawnedPowerups.erase(spawnedPowerups.begin() + index);
		delete powerup;
	}
}


