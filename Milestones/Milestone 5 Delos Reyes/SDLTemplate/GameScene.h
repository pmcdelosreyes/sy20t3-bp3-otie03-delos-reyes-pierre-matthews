#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include <vector>
#include "text.h"
#include "Powerup.h"

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();

private:
	Player* player;

	// Enemy spawning logic
	float spawnTime;
	float currentSpawnTimer;
	std::vector<Enemy*> spawnedEnemies;
	std::vector<Powerup*> spawnedPowerups;
	void doSpawnLogic();
	void doCollisionLogic();
	void doPowerupCollision();
	void spawn();
	void despawnEnemy(Enemy* enemy);
	void spawnPowerup();
	void despawnPowerup(Powerup* powerup);

	int points;
};

